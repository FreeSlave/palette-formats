# Palette formats

This repository contains the set of palette (color table) loaders for different formats. It can be used in Qt4 and Qt5 projects.

## Supported formats

### Microsoft palette

Binary palette format used by some games and Windows applications (.pal). Color table usually have 256 colors with no alpha channel. Every color has *flags* component, which is often equal to 0, but can be used to store some additional information (for example, alpha value).

### JASC palette

Text palette format used by Paintshop and some other applications (.pal, don't be confused with Microsoft palettes, which use the same extension). Does not have alpha channel.

### GIMP palette

Text palette format used by GNU Image Manipulation Program (.gpl). Does not have alpha channel.

### Paint.NET palette

Text palette format used by Paint.NET (.txt). Has alpha channel. Count of colors is always equal to 96. Bigger color tables will be truncated, smaller ones will be supplemented with white (FF FF FF FF) colors.

### Photoshop color swatches

Binary palette format used by Photoshop (.aco). Does not have alpha channel. Note that current support for Photoshop color swatches is incomplete (there is lack of support for some rarely used color spaces) and may be lossy (since Photoshop colors have higher precision than plain RGB).

## Example 

See example in 'example' directory.

## License

See UNLICENSE for license information.
