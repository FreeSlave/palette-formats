#include "paletteloaderwidget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    PaletteLoaderWidget w;
    w.show();

    return a.exec();
}
