#-------------------------------------------------
#
# Project created by QtCreator 2014-08-28T19:36:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = paletteformats
TEMPLATE = app


SOURCES += main.cpp\
    ../src/microsoftpalette.cpp \
    ../src/paintdotnetpalette.cpp \
    ../src/jascpalette.cpp \
    ../src/photoshopswatch.cpp \
    ../src/gimppalette.cpp \
    ../src/palettehandler.cpp \
    ../src/jascpalettehandler.cpp \
    ../src/microsoftpalettehandler.cpp \
    ../src/paintdotnetpalettehandler.cpp \
    ../src/photoshopswatchhandler.cpp \
    ../src/gimppalettehandler.cpp \
    paletteloaderwidget.cpp \
    palettewidget.cpp

HEADERS  += \
    ../src/paintdotnetpalette.h \
    ../src/jascpalette.h \
    ../src/photoshopswatch.h \
    ../src/gimppalette.h \
    ../src/palettehandler.h \
    ../src/jascpalettehandler.h \
    ../src/microsoftpalettehandler.h \
    ../src/paintdotnetpalettehandler.h \
    ../src/photoshopswatchhandler.h \
    ../src/microsoftpalette.h \
    ../src/gimppalettehandler.h \
    paletteloaderwidget.h \
    palettewidget.h

INCLUDEPATH = ../src
