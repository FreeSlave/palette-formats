#include "paletteloaderwidget.h"

#include <QPushButton>
#include <QFileDialog>
#include <QMessageBox>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>

#include "microsoftpalettehandler.h"
#include "paintdotnetpalettehandler.h"
#include "jascpalettehandler.h"
#include "photoshopswatchhandler.h"
#include "gimppalettehandler.h"

PaletteLoaderWidget::PaletteLoaderWidget(QWidget *parent)
    : QWidget(parent)
{
    QPushButton* readFileCmd = new QPushButton("Open");
    QPushButton* writeFileCmd = new QPushButton("Save");

    _paletteHandlers.append(new GIMPPaletteHandler);
    _paletteHandlers.append(new MicrosoftPaletteHandler);
    _paletteHandlers.append(new JASCPaletteHandler);
    _paletteHandlers.append(new PaintDotNetPaletteHandler);
    _paletteHandlers.append(new PhotoshopSwatchHandler);

    _formatBox = new QComboBox;

    foreach (PaletteHandler* handler, _paletteHandlers) {
        _formatBox->addItem(handler->name(), makeFilterString(handler));
    }

    QHBoxLayout* hbox = new QHBoxLayout;
    hbox->addWidget(readFileCmd);
    hbox->addWidget(_formatBox);
    hbox->addWidget(writeFileCmd);
    hbox->addStretch();

    _currentIndexLabel = new QLabel;
    _colorEdit = new QLineEdit;

    QFormLayout* formLayout = new QFormLayout;
    formLayout->addRow(tr("Index: "), _currentIndexLabel);
    formLayout->addRow(tr("Color: "), _colorEdit);

    QVector<QRgb> pal(256);
    for (int i=0; i<pal.size(); ++i)
    {
        pal[i] = qRgb(0xFF, 0xFF, 0xFF);
    }

    _palWidget = new PaletteWidget(pal);
    _palWidget->setFocusPolicy(Qt::StrongFocus);
    _palWidget->setMinimumSize(_palWidget->cellSize().width() * 16, _palWidget->cellSize().height() * 16);

    QHBoxLayout* hbox2 = new QHBoxLayout;
    hbox2->addWidget(_palWidget,1);
    hbox2->addLayout(formLayout);

    QVBoxLayout* vbox = new QVBoxLayout;
    vbox->addLayout(hbox);
    vbox->addLayout(hbox2);

    setLayout(vbox);

    connect(readFileCmd, SIGNAL(clicked()), SLOT(readFile()));
    connect(writeFileCmd, SIGNAL(clicked()), SLOT(writeFile()));

    connect(_palWidget, SIGNAL(colorChosen(int,QRgb)), SLOT(colorSlot(int,QRgb)));
    connect(_colorEdit, SIGNAL(editingFinished()), SLOT(changeCurrentColor()));
}

PaletteLoaderWidget::~PaletteLoaderWidget()
{
    for (int i=0; i<_paletteHandlers.size(); ++i)
        delete _paletteHandlers[i];
}

QString PaletteLoaderWidget::makeFilterString(const PaletteHandler *handler)
{
    return QString("%1 (*.%2)").arg(handler->name(), handler->extensions().join(" *."));
}

void PaletteLoaderWidget::readFile()
{
    int index = _formatBox->currentIndex();
    if (index < 0 || index >= _paletteHandlers.size())
    {
        QMessageBox::critical(this, "Error", "No palette type selected");
    }

    QString fileName = QFileDialog::getOpenFileName(this, "Open file", QString(), QString("%1;;Any (*)").arg(_formatBox->itemData(index).toString()));
    if (!fileName.isEmpty())
    {
        QVector<QRgb> pal = _paletteHandlers[index]->readFromFile(fileName);
        if (pal.isEmpty())
        {
            QMessageBox::critical(this, "Error", "Failed to read palette file");
        }
        else
        {
            _palWidget->setColorTable(pal);
        }
    }
}

void PaletteLoaderWidget::writeFile()
{
    int index = _formatBox->currentIndex();
    if (index < 0 || index >= _paletteHandlers.size())
    {
        QMessageBox::critical(this, "Error", "No palette type selected");
    }
    QString fileName = QFileDialog::getSaveFileName(this, "Save file", QString(), QString("%1;;Any (*)").arg(_formatBox->itemData(index).toString()));
    if (!fileName.isEmpty())
    {
        bool result = _paletteHandlers[index]->writeToFile(fileName, _palWidget->colorTable());
        if (!result)
        {
            QMessageBox::critical(this, "Error", "Failed to write palette file");
        }
        else
        {
            QMessageBox::information(this, "Succeed", "Palette was saved successfully");
        }
    }
}

void PaletteLoaderWidget::colorSlot(int index, QRgb rgb)
{
    if (index >= 0)
    {
        _currentIndexLabel->setText(QString::number(index));
        _colorEdit->setText(colorToText(rgb));
    }
    else
    {
        _currentIndexLabel->clear();
        _colorEdit->clear();
    }
}

void PaletteLoaderWidget::changeCurrentColor()
{
    QColor color = colorFromText(_colorEdit->text());
    if (color.isValid())
        _palWidget->setColor(_palWidget->currentIndex(), color.rgba());
}

QString PaletteLoaderWidget::colorToText(const QRgb &rgb)
{
    return QString("%1 %2 %3 %4").arg(qRed(rgb)).arg(qGreen(rgb)).arg(qBlue(rgb)).arg(qAlpha(rgb));
}

QColor PaletteLoaderWidget::colorFromText(const QString &text)
{
    QStringList list = text.split(' ', QString::SkipEmptyParts);
    QColor toReturn;
    if (list.size() >= 3 && list.size() <= 4)
    {
        bool ok;
        int red = list.at(0).toInt(&ok);
        if (!ok || red < 0 || red > 255)
            return toReturn;
        int green = list.at(1).toInt(&ok);
        if (!ok || green < 0 || green > 255)
            return toReturn;
        int blue = list.at(2).toInt(&ok);
        if (!ok || blue < 0 || blue > 255)
            return toReturn;

        if (list.size() == 4)
        {
            int alpha = list.at(3).toInt(&ok);
            if (!ok || alpha < 0 || alpha > 255)
                return toReturn;
            toReturn.setAlpha(alpha);
        }

        toReturn.setRed(red);
        toReturn.setGreen(green);
        toReturn.setBlue(blue);

        return toReturn;
    }
    return toReturn;
}

