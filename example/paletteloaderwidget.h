#ifndef PALETTELOADERWIDGET_H
#define PALETTELOADERWIDGET_H

#include <QVector>
#include <QColor>
#include <QWidget>
#include <QComboBox>
#include <QList>
#include <QLabel>
#include <QLineEdit>

#include "palettewidget.h"
#include "palettehandler.h"

class PaletteLoaderWidget : public QWidget
{
    Q_OBJECT

public:
    PaletteLoaderWidget(QWidget *parent = 0);
    ~PaletteLoaderWidget();

public slots:
    void readFile();
    void writeFile();
    void colorSlot(int index, QRgb rgb);
    void changeCurrentColor();

private:
    static QColor colorFromText(const QString& text);
    static QString colorToText(const QRgb& rgb);
    static QString makeFilterString(const PaletteHandler* handler);

    QComboBox* _formatBox;
    QList<PaletteHandler*> _paletteHandlers;



    QLabel* _currentIndexLabel;
    QLineEdit* _colorEdit;

    PaletteWidget* _palWidget;
};

#endif //PALETTELOADERWIDGET_H
