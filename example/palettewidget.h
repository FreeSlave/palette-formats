#ifndef PALETTEWIDGET_H
#define PALETTEWIDGET_H

#include <QWidget>
#include <QVector>
#include <QColor>
#include <QPen>
#include <QSize>

/// The PaletteWidget class is class for viewing color tables and picking particular color
/**
 * Color table is drawing as table of colored cells. The size of each cell depends on cell size policy. By default cells have fixed size, which you may change by calling setCellSize(). Use setFixedCellSize(false) to change this behavior and force calculating of cell's size based on widget size.
 * By default user can select color only by mouse. Use QWidget::setFocusPolicy() to allow keyboard input with arrows buttons.
 */

class PaletteWidget : public QWidget
{
    Q_OBJECT
public:
    /**
     * Constructs PaletteWidget with empty color table
     * @param parent parent of widget
     */
    PaletteWidget(QWidget *parent = 0);

    /**
     * @param colorTable array of rgba values.
     * @param parent parent of widget.
     * @sa setColorTable()
     */
    PaletteWidget(const QVector<QRgb>& colorTable, QWidget *parent = 0);
    ~PaletteWidget();

    /**
     * Sets new color table and updates widget. Resets selection.
     * @sa colorTable()
     */
    void setColorTable(const QVector<QRgb>& colorTable);

    /**
     * @return current color table
     * @sa setColorTable()
     */
    QVector<QRgb> colorTable() const {
        return _colorTable;
    }

    /**
     * @return count of colors in the color table
     * @sa resizeColorTable()
     */
    int colorCount() const {
        return _colorTable.size();
    }

    /**
     * Resizes color table and updates widget if necessarily. Resets selection.
     * @sa colorCount(), setColorTable()
     */
    void resizeColorTable(int i);

    /**
     * @param index index of color. Must be value between 0 and colorCount().
     * @return rgba value corresponded to index
     * @sa colorCount(), setColor()
     */
    QRgb color(int index) const {
        return _colorTable.at(index);
    }

    /**
     * @return size of cell which represents the color in the color table.
     *  If isFixedCellSize() is true it returns value set by setCellSize or default value, otherwise calculates cell size based on widget size
     * @sa setCellSize()
     */
    QSize cellSize() const;

    /**
     * Sets cell size and if isFixedCellSize() is true updates the widget
     * @param size new size of cell.
     * @sa cellSize()
     */
    void setCellSize(const QSize& size);

    /**
     * Reimplemented from QWidget. When using fixed cell size, it's good idea to set QWidget::setMinimumSize() to sizeHint() value or put PaletteWidget to QScrollArea.
     * @sa isFixedCellSize()
     */
    QSize sizeHint() const;

    /**
     * @return true if widget has fixed cell size, otherwise false.
     */
    bool isFixedCellSize() const {
        return _fixedCellSize;
    }

    /**
     * @return currently selected color or 0 if no color selected
     * @sa currentIndex()
     */
    QRgb currentColor() const {
        if (currentIndex() >= 0)
            return color(currentIndex());
        return 0;
    }

    /**
     * @return index of currently selected color or negative value if no color selected
     * @sa currentColor(), setCurrent()
     */
    int currentIndex() const {
        return _chosen;
    }

    /**
     * @return QPen which is used to draw frame around the selected color cell
     * @sa setSelectionPen()
     */
    QPen selectionPen() const {
        return _selectionPen;
    }

    /**
     * Sets pen which will be used to draw frame around the selected color cell and updates widget is necessarily.
     * Note that QPen::color() property will be ignored during drawing since color is calculated based on cell's color for better visibility of the frame. Rewrite or reimplement in derived class oppositeColor() function to change this behaviour.
     * @sa selectionPen()
     */
    void setSelectionPen(const QPen& selectionPen);

    /**
     * @return QPen which is used to draw frame around color cells
     * @sa setFramePen()
     */
    QPen framePen() const {
        return _framePen;
    }

    /**
     * Sets pen which will be used to draw frame around color cells. Pass QPen(Qt::NoPen) to remove frames.
     * @sa framePen()
     */
    void setFramePen(const QPen& framePen);

    /**
     * Returns column number previously set by setColumnNumber(). If 0 was set (it's default) it calculates "optimal" number of columns in color table which will be used while drawing. The result depends on the cell size policy.
     * @sa setColumnNumber()
     */
    int columnNumber() const;

    /**
     * @return true if widget has fixed column number set by setColumnNumber(), otherwise false
     */
    bool isFixedColumnNumber() const {
        return _columnNumber != 0;
    }

public slots:
    /**
     * Changes color in the color table. Updates widget if necessarily. Color table should be set.
     * @param index index of color to change. Should be value between 0 and colorCount(), otherwise will be ignored.
     * @param color rgba value of color to put in color table.
     * @sa color()
     */
    void setColor(int index, QRgb rgb);

    /**
     * Sets current chosen index and updates widget is necessarily. Emits colorChosen() signal if current index was changed.
     * @sa currentColor(), currentIndex(), resetSelection()
     */
    void setCurrent(int index);

    /**
     * Sets cell size policy and updates widget if necessarily.
     */
    void setFixedCellSize(bool fixed);

    /**
     * Resets current cell selection (i.e. no color is selected after calling this function)
     */
    void resetSelection();

    /**
     * Sets fixed color number. 0 means column number will be calculated. Update widget if necessarily.
     * @sa columnNumber()
     */
    void setColumnNumber(int columnNumber);

signals:
    /**
     * Emitted when current chosen index is changed by user (through mouse/keyboard input) or programmatically.
     */
    void colorChosen(int, QRgb);
protected:
    void keyPressEvent(QKeyEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void paintEvent(QPaintEvent* event);

    /**
     * @return "opposite" color. The definition of "opposite" is informal. In this case it means color which is clearly visible on the background of another color.
     */
    virtual QColor oppositeColor(const QColor& color) const;

    /**
     * @param pos position on the widget
     * @return index of picked color or -1 if no cell placed under this position.
     * @sa posFromIndex()
     */
    int indexFromPos(const QPoint& pos) const;

    /**
     * @param index index of color in color table
     * @return left-top point of cell which represents indexed color or QPoint(-1,-1) if index is not valid.
     * @sa indexFromPos()
     */
    QPoint posFromIndex(int index) const;

    /**
     * @return rounded up square root
     */
    static int integerSqrt(int i);

    int cellsX() const;
    int cellsY() const;
    int cellsWidth() const;
    int cellsHeight() const;

private:
    void construct();

    int _chosen;
    QSize _cellSize;
    QPen _selectionPen;
    QPen _framePen;
    QVector<QRgb> _colorTable;
    int _columnNumber;
    bool _fixedCellSize;
};

#endif // PALETTEWIDGET_H
