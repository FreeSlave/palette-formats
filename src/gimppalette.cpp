#include "gimppalette.h"
#include <cstdio>
#include <cstdlib>

QVector<QRgb> GIMPPalette::readFromDevice(QIODevice* device)
{
    if (device && device->isOpen() && device->isReadable())
    {
        char buf[256] = {'\0'};
        do
        {
            if (device->readLine(buf, sizeof(buf)) <= 0)
                return QVector<QRgb>();
        }
        while(buf[0] != '#');

        do
        {
            if (device->readLine(buf, sizeof(buf)) <= 0)
                return QVector<QRgb>();
        }
        while(buf[0] == '#');

        QVector<QRgb> pal;
        qint64 n;
        do
        {
            char* endptr;
            long red = strtol(buf, &endptr, 10);
            long green = strtol(endptr, &endptr, 10);
            long blue = strtol(endptr, NULL, 10);

            if (red < 0 || red >= 256 || green < 0 || green >= 256 || blue < 0 || blue >= 256)
                return QVector<QRgb>();

            pal.append(qRgb(red, green, blue));

            n = device->readLine(buf, sizeof(buf));
        }
        while(n > 0);

        return pal;
    }
    return QVector<QRgb>();
}

bool GIMPPalette::writeToDevice(QIODevice* device, const QVector<QRgb>& pal)
{
    if (device && device->isOpen() && device->isWritable())
    {
        device->write("GIMP Palette\n");
        device->write("Name: Unnamed\n");
        device->write("#\n");

        char buf[256] = {'\0'};
        for (int i=0; i<pal.size(); ++i)
        {
            QRgb rgb = pal[i];
            sprintf(buf, "%3d %3d %3d\n", qRed(rgb), qGreen(rgb), qBlue(rgb));
            if (device->write(buf) <= 0)
                return false;
        }
        return true;
    }
    return false;
}
