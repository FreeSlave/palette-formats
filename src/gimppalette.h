#ifndef GIMPPALETTE_H
#define GIMPPALETTE_H

#include <QVector>
#include <QColor>
#include <QIODevice>

struct GIMPPalette
{
    static QVector<QRgb> readFromDevice(QIODevice* device);
    static bool writeToDevice(QIODevice* device, const QVector<QRgb>& pal);
};

#endif // GIMPPALETTE_H
