#include "gimppalettehandler.h"
#include "gimppalette.h"

GIMPPaletteHandler::GIMPPaletteHandler()
{
}

QVector<QRgb> GIMPPaletteHandler::readFromDevice(QIODevice *device)
{
    return GIMPPalette::readFromDevice(device);
}

bool GIMPPaletteHandler::writeToDevice(QIODevice *device, const QVector<QRgb> &pal)
{
    return GIMPPalette::writeToDevice(device, pal);
}

QString GIMPPaletteHandler::name() const
{
    return "GIMP Palette";
}

QStringList GIMPPaletteHandler::extensions() const
{
    return QStringList() << "gpl";
}
