#ifndef GIMPPALETTEHANDLER_H
#define GIMPPALETTEHANDLER_H

#include "palettehandler.h"

class GIMPPaletteHandler : public PaletteHandler
{
public:
    GIMPPaletteHandler();
    QVector<QRgb> readFromDevice(QIODevice *device);
    bool writeToDevice(QIODevice *device, const QVector<QRgb> &pal);
    QString name() const;
    QStringList extensions() const;
};

#endif // GIMPPALETTEHANDLER_H
