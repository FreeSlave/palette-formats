#ifndef JASCPALETTE_H
#define JASCPALETTE_H

#include <QVector>
#include <QColor>
#include <QIODevice>

struct JASCPalette
{
    static QVector<QRgb> readFromDevice(QIODevice* device);
    static bool writeToDevice(QIODevice* device, const QVector<QRgb>& pal);
};

#endif // JASCPALETTE_H
