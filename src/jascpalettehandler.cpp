#include "jascpalettehandler.h"
#include "jascpalette.h"

JASCPaletteHandler::JASCPaletteHandler()
{
}

QVector<QRgb> JASCPaletteHandler::readFromDevice(QIODevice *device)
{
    return JASCPalette::readFromDevice(device);
}

bool JASCPaletteHandler::writeToDevice(QIODevice *device, const QVector<QRgb> &pal)
{
    return JASCPalette::writeToDevice(device, pal);
}

QString JASCPaletteHandler::name() const
{
    return "JASC Palette";
}

QStringList JASCPaletteHandler::extensions() const
{
    return QStringList() << "txt" << "pal" << "jasc";
}
