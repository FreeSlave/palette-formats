#ifndef JASCPALETTEHANDLER_H
#define JASCPALETTEHANDLER_H

#include "palettehandler.h"

class JASCPaletteHandler : public PaletteHandler
{
public:
    JASCPaletteHandler();
    QVector<QRgb> readFromDevice(QIODevice *device);
    bool writeToDevice(QIODevice *device, const QVector<QRgb> &pal);
    QString name() const;
    QStringList extensions() const;
};

#endif // JASCPALETTEHANDLER_H
