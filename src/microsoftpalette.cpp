#include "microsoftpalette.h"

#include <QDataStream>
#include <cstring>

namespace
{
const char* const RIFF = "RIFF";
const char* const PAL = "PAL ";
}

QVector<QRgb> MicrosoftPalette::readFromDevice(QIODevice *device, bool flagsAsAlpha)
{
    if (device && device->isOpen() && device->isReadable())
    {
        QDataStream stream(device);
        stream.setByteOrder(QDataStream::LittleEndian);

        Header header;

        int n = stream.readRawData(reinterpret_cast<char*>(&header), sizeof(header));
        if (n != sizeof(header) || strncmp(header.riff, RIFF, 4) != 0 || strncmp(header.type, PAL, 4) != 0)
            return QVector<QRgb>();

        if (header.entries <= 0)
            header.entries = 256;

        QVector<QRgb> toReturn(header.entries);

        for (int i=0; i<header.entries; ++i)
        {
            quint8 red, green, blue, flags;
            stream >> red >> green >> blue >> flags;

            if (stream.status() != QDataStream::Ok)
                return QVector<QRgb>();

            if (flagsAsAlpha)
                toReturn[i] = qRgba(red, green, blue, flags);
            else
                toReturn[i] = qRgb(red, green, blue);
        }

        return toReturn;
    }

    return QVector<QRgb>();
}

bool MicrosoftPalette::writeToDevice(QIODevice *device, const QVector<QRgb> &pal, bool alphaAsFlags)
{
    if (device && device->isOpen() && device->isWritable())
    {
        QDataStream stream(device);
        stream.setByteOrder(QDataStream::LittleEndian);

        int bytesSize = pal.size() * 4 + sizeof(Header);

        Header header;
        strncpy(header.riff, RIFF, 4);
        header.dataSize = bytesSize - 8;
        strncpy(header.type, PAL, 4);
        strncpy(header.data, "data", 4);
        header.chunkSize = bytesSize - 20;
        header.version = 0x300;
        header.entries = pal.size();

        int n = stream.writeRawData(reinterpret_cast<const char*>(&header), sizeof(header));
        if (n != sizeof(header))
            return false;

        for (int i=0; i<pal.size(); ++i)
        {
            QRgb rgb = pal.at(i);
            quint8 red = static_cast<quint8>(qRed(rgb));
            quint8 green = static_cast<quint8>(qGreen(rgb));
            quint8 blue = static_cast<quint8>(qBlue(rgb));
            quint8 alpha = static_cast<quint8>(qAlpha(rgb));

            stream << red << green << blue << (alphaAsFlags ? alpha : static_cast<quint8>(0));

            if (stream.status() != QDataStream::Ok)
                return false;
        }
        return true;
    }
    return false;
}
