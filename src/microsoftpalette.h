#ifndef MICROSOFTPALETTE_H
#define MICROSOFTPALETTE_H

#include <QVector>
#include <QColor>
#include <QIODevice>

struct MicrosoftPalette
{
    struct Header
    {
        char riff[4];
        qint32 dataSize;
        char type[4];
        char data[4];
        qint32 chunkSize;
        qint16 version;
        qint16 entries;
    };

    static QVector<QRgb> readFromDevice(QIODevice* device, bool flagsAsAlpha = false);
    static bool writeToDevice(QIODevice* device, const QVector<QRgb>& pal, bool alphaAsFlags = false);
};



#endif // MICROSOFTPALETTE_H
