#include "microsoftpalettehandler.h"
#include "microsoftpalette.h"

MicrosoftPaletteHandler::MicrosoftPaletteHandler()
{
}

QVector<QRgb> MicrosoftPaletteHandler::readFromDevice(QIODevice *device)
{
    return MicrosoftPalette::readFromDevice(device);
}

bool MicrosoftPaletteHandler::writeToDevice(QIODevice *device, const QVector<QRgb> &pal)
{
    return MicrosoftPalette::writeToDevice(device, pal);
}

QString MicrosoftPaletteHandler::name() const
{
    return "Microsoft Palette";
}

QStringList MicrosoftPaletteHandler::extensions() const
{
    return QStringList() << "pal";
}
