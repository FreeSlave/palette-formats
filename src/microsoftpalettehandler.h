#ifndef MICROSOFTPALETTEHANDLER_H
#define MICROSOFTPALETTEHANDLER_H

#include "palettehandler.h"

class MicrosoftPaletteHandler : public PaletteHandler
{
public:
    MicrosoftPaletteHandler();
    QVector<QRgb> readFromDevice(QIODevice *device);
    bool writeToDevice(QIODevice *device, const QVector<QRgb> &pal);
    QString name() const;
    QStringList extensions() const;
};

#endif // MICROSOFTPALETTEHANDLER_H
