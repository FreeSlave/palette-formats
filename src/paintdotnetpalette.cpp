#include "paintdotnetpalette.h"

#include <cstring>

QVector<QRgb> PaintDotNetPalette::readFromDevice(QIODevice *device)
{
    if (device && device->isOpen() && device->isReadable())
    {
        char buf[1024];
        QVector<QRgb> pal(96);

        int i = 0;
        while(!device->atEnd() && i != pal.size())
        {
            qint64 n = device->readLine(buf, sizeof(buf));
            if (n > 0)
            {
                if (buf[0] == ';')
                    continue;
                else if (n >= 8)
                {
                    bool ok;
                    QRgb rgb = hex(buf, &ok);
                    if (!ok)
                        return QVector<QRgb>();
                    pal[i] = rgb;
                    i++;
                }
                else
                    return QVector<QRgb>();
            }
        }

        if (i != pal.size())
        {
            for (;i<pal.size(); ++i)
                pal[i] = qRgba(0xFF, 0xFF, 0xFF, 0xFF);
        }

        return pal;
    }
    return QVector<QRgb>();
}

bool PaintDotNetPalette::writeToDevice(QIODevice *device, const QVector<QRgb> &pal)
{
    if (device && device->isOpen() && device->isWritable())
    {
        char buf[8];
        int min = qMin(pal.size(), 96);
        for (int i=0; i<min; ++i)
        {
            rgbToStr(buf, pal[i]);
            if (device->write(buf, sizeof(buf)) != sizeof(buf))
                return false;
            if (device->write("\r\n", 2) != 2)
                return false;
        }

        for (int i=pal.size(); i<96; ++i)
        {
            rgbToStr(buf, qRgba(0xFF, 0xFF, 0xFF, 0xFF));
            if (device->write(buf, sizeof(buf)) != sizeof(buf))
                return false;
            if (device->write("\r\n", 2) != 2)
                return false;
        }
        return true;
    }
    return false;
}

quint8 PaintDotNetPalette::hex(char first, char second, bool *ok)
{
    quint8 f = hex(first, ok);
    if (!*ok)
        return 0;
    quint8 s = hex(second, ok);
    if (!*ok)
        return 0;
    return f*16 + s;
}

quint8 PaintDotNetPalette::hex(char c, bool *ok)
{
    *ok = true;
    if (c >= '0' && c <= '9')
        return c - '0';
    switch (c)
    {
    case 'A': case 'a': return 10;
    case 'B': case 'b': return 11;
    case 'C': case 'c': return 12;
    case 'D': case 'd': return 13;
    case 'E': case 'e': return 14;
    case 'F': case 'f': return 15;
    default:
        *ok = false;
        return 0;
        break;
    }
}

QRgb PaintDotNetPalette::hex(const char *s, bool *ok)
{
    quint8 alpha = hex(s[0], s[1], ok);
    if (!*ok)
        return 0;
    quint8 red = hex(s[2], s[3], ok);
    if (!*ok)
        return 0;
    quint8 green = hex(s[4], s[5], ok);
    if (!*ok)
        return 0;
    quint8 blue = hex(s[6], s[7], ok);
    if (!*ok)
        return 0;

    return qRgba(red, green, blue, alpha);
}

char PaintDotNetPalette::hex(quint8 value)
{
    static const char hexmap[] = {'0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'};
    return hexmap[value];
}

void PaintDotNetPalette::hex(char &first, char &second, quint8 value)
{
    first = hex(value / 16);
    second = hex(value % 16);
}

void PaintDotNetPalette::rgbToStr(char *buf, QRgb value)
{
    hex(buf[0], buf[1], static_cast<quint8>(qAlpha(value)));
    hex(buf[2], buf[3], static_cast<quint8>(qRed(value)));
    hex(buf[4], buf[5], static_cast<quint8>(qGreen(value)));
    hex(buf[6], buf[7], static_cast<quint8>(qBlue(value)));
}
