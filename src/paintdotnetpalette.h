#ifndef PAINTDOTNETPALETTE_H
#define PAINTDOTNETPALETTE_H

#include <QVector>
#include <QColor>
#include <QIODevice>

struct PaintDotNetPalette
{
    static QVector<QRgb> readFromDevice(QIODevice* device);
    static bool writeToDevice(QIODevice* device, const QVector<QRgb>& pal);
private:
    static quint8 hex(char first, char second, bool* ok);
    static quint8 hex(char c, bool* ok);
    static QRgb hex(const char* s, bool* ok);
    static void rgbToStr(char* buf, QRgb value);
    static void hex(char& first, char& second, quint8 value);
    static char hex(quint8 value);
};

#endif // PAINTDOTNETPALETTE_H
