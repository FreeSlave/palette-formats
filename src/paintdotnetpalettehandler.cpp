#include "paintdotnetpalettehandler.h"
#include "paintdotnetpalette.h"

PaintDotNetPaletteHandler::PaintDotNetPaletteHandler()
{
}

QVector<QRgb> PaintDotNetPaletteHandler::readFromDevice(QIODevice *device)
{
    return PaintDotNetPalette::readFromDevice(device);
}

bool PaintDotNetPaletteHandler::writeToDevice(QIODevice *device, const QVector<QRgb> &pal)
{
    return PaintDotNetPalette::writeToDevice(device, pal);
}

QString PaintDotNetPaletteHandler::name() const
{
    return "Paint.NET Palette";
}

QStringList PaintDotNetPaletteHandler::extensions() const
{
    return QStringList() << "txt";
}
