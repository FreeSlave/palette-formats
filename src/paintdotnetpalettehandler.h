#ifndef PAINTDOTNETPALETTEHANDLER_H
#define PAINTDOTNETPALETTEHANDLER_H

#include "palettehandler.h"

class PaintDotNetPaletteHandler : public PaletteHandler
{
public:
    PaintDotNetPaletteHandler();
    QVector<QRgb> readFromDevice(QIODevice *device);
    bool writeToDevice(QIODevice *device, const QVector<QRgb> &pal);
    QString name() const;
    QStringList extensions() const;
};

#endif // PAINTDOTNETPALETTEHANDLER_H
