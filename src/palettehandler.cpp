#include "palettehandler.h"
#include <QFile>

PaletteHandler::PaletteHandler()
{

}

PaletteHandler::~PaletteHandler()
{

}

QVector<QRgb> PaletteHandler::readFromFile(const QString &fileName)
{
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly))
    {
        return readFromDevice(&file);
    }
    return QVector<QRgb>();
}

bool PaletteHandler::writeToFile(const QString &fileName, const QVector<QRgb> &pal)
{
    QFile file(fileName);
    if (file.open(QIODevice::WriteOnly))
    {
        return writeToDevice(&file, pal);
    }
    return false;
}
