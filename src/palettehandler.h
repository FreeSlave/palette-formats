#ifndef PALETTEHANDLER_H
#define PALETTEHANDLER_H

#include <QVector>
#include <QColor>
#include <QString>
#include <QStringList>
#include <QIODevice>

class PaletteHandler
{
public:
    PaletteHandler();
    virtual ~PaletteHandler();

    virtual QVector<QRgb> readFromDevice(QIODevice* device) = 0;
    virtual QVector<QRgb> readFromFile(const QString& fileName);

    virtual bool writeToDevice(QIODevice* device, const QVector<QRgb>& pal) = 0;
    virtual bool writeToFile(const QString& fileName, const QVector<QRgb>& pal);

    virtual QString name() const = 0;
    virtual QStringList extensions() const = 0;
};

#endif // PALETTEHANDLER_H
