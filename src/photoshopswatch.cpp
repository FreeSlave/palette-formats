#include "photoshopswatch.h"

#include <QDataStream>
#include <cstring>

QVector<QRgb> PhotoshopSwatch::readFromDevice(QIODevice *device)
{
    if (device && device->isOpen() && device->isReadable())
    {
        QDataStream stream(device);
        stream.setByteOrder(QDataStream::BigEndian);
        short version;
        short count;

        stream >> version >> count;

        if (stream.status() != QDataStream::Ok)
            return QVector<QRgb>();

        if (version != 1)
            return QVector<QRgb>();
        if (count <= 0)
            return QVector<QRgb>();

        QVector<QRgb> pal(count);
        for (int i=0; i<count; ++i)
        {
            unsigned short space, w, x, y, z;
            stream >> space >> w >> x >> y >> z;

            if (stream.status() != QDataStream::Ok)
                return QVector<QRgb>();

            switch(space)
            {
            case 0:
                pal[i] = qRgb(w/256, x/256, y/256);
                break;
            case 1:
                pal[i] = QColor::fromHsv(qRound(w/182.04), qRound(x/655.35), qRound(y/655.35)).rgba();
                break;
            case 2:
                pal[i] = QColor::fromCmyk(qRound(100-w/655.35), qRound(100-x/655.35), qRound(100-y/655.35), qRound(100-z/655.35)).rgba();
                break;
            case 7:
                return QVector<QRgb>(); //Lab color space not implemented
                break;
            case 8:
                pal[i] = qRgb(qRound(w/39.0625), qRound(x/39.0625), qRound(y/39.0625));
                break;
            case 9:
                return QVector<QRgb>(); //Wide CMYK color space not implemented
                break;
            default:
                return QVector<QRgb>(); //unknown color space
                break;
            }
        }
        return pal;
    }
    return QVector<QRgb>();
}

bool PhotoshopSwatch::writeToDevice(QIODevice *device, const QVector<QRgb> &pal)
{
    if (device && device->isOpen() && device->isWritable())
    {
        QDataStream stream(device);
        stream.setByteOrder(QDataStream::BigEndian);

        short version = 1;
        short count = static_cast<short>(pal.size());

        stream << version << count;

        if (stream.status() != QDataStream::Ok)
            return false;

        for (int i=0; i<count; ++i)
        {
            QRgb rgb = pal[i];
            unsigned short space = 0;
            unsigned short red = qRed(rgb)*256;
            unsigned short green = qGreen(rgb)*256;
            unsigned short blue = qBlue(rgb)*256;
            unsigned short z = 0;

            stream << space << red << green << blue << z;

            if (stream.status() != QDataStream::Ok)
                return false;
        }
        return true;
    }
    return false;
}
