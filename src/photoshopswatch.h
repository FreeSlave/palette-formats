#ifndef PHOTOSHOPSWATCH_H
#define PHOTOSHOPSWATCH_H

#include <QVector>
#include <QColor>
#include <QIODevice>

struct PhotoshopSwatch
{
    static QVector<QRgb> readFromDevice(QIODevice* device);
    static bool writeToDevice(QIODevice* device, const QVector<QRgb>& pal);
};

#endif // PHOTOSHOPSWATCH_H
