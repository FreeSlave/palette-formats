#include "photoshopswatchhandler.h"
#include "photoshopswatch.h"

PhotoshopSwatchHandler::PhotoshopSwatchHandler()
{
}

QVector<QRgb> PhotoshopSwatchHandler::readFromDevice(QIODevice *device)
{
    return PhotoshopSwatch::readFromDevice(device);
}

bool PhotoshopSwatchHandler::writeToDevice(QIODevice *device, const QVector<QRgb> &pal)
{
    return PhotoshopSwatch::writeToDevice(device, pal);
}

QString PhotoshopSwatchHandler::name() const
{
    return "Photoshop Swatch";
}

QStringList PhotoshopSwatchHandler::extensions() const
{
    return QStringList() << "aco";
}
