#ifndef PHOTOSHOPSWATCHHANDLER_H
#define PHOTOSHOPSWATCHHANDLER_H

#include "palettehandler.h"

class PhotoshopSwatchHandler : public PaletteHandler
{
public:
    PhotoshopSwatchHandler();
    QVector<QRgb> readFromDevice(QIODevice *device);
    bool writeToDevice(QIODevice *device, const QVector<QRgb> &pal);
    QString name() const;
    QStringList extensions() const;
};

#endif // PHOTOSHOPSWATCHHANDLER_H
